// Copyright  (C)  2015  Dominick Vanthienen <dominick dot vanthienen at mech dot kuleuven dot be>

// Version: 1.0
// Author: Dominick Vanthienen
// Maintainer: Dominick Vanthienen
// URL: http://www.orocos.org/kdl
// based on ChainIdSolver_RNE by Ruben Smits
// Copyright  (C)  2009  Ruben Smits <ruben dot smits at mech dot kuleuven dot be>

// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef KDL_CHAIN_IKSOLVER_GRAVITY_HPP
#define KDL_CHAIN_IKSOLVER_GRAVITY_HPP

#include <kdl/chainidsolver.hpp>

namespace KDL{
    class ChainIdSolver_gravity : public ChainIdSolver{
    public:
        /** 
         * Constructor for the solver, it will allocate all the necessary memory
         * \param chain The kinematic chain to calculate the inverse dynamics for, an internal copy will be made.
         * \param grav The gravity vector to use during the calculation.
         */
        ChainIdSolver_gravity(const Chain& chain,Vector grav);
        ~ChainIdSolver_gravity(){};

        //ChainIdSolver class demands implementation of the following:
        int CartToJnt(const JntArray &q, const JntArray &q_dot, const JntArray &q_dotdot, const Wrenches& f_ext,JntArray &torques){
          return CartToJnt(q, torques);
        };
     
        /** 
         * Function to calculate from gravity forces to joint torques.
         * Input parameters;
         * \param q The current joint positions
         * Output parameters:
         * \param torques the resulting torques for the joints
         */
        int CartToJnt(const JntArray &q, JntArray &torques);

    private:
        Chain chain;
        //number of joints
        unsigned int nj;
        //number of segments
        unsigned int ns;
        std::vector<Frame> X;
        std::vector<Twist> S;
        std::vector<Twist> v;
        std::vector<Twist> a;
        std::vector<Wrench> f;
        Twist ag;
        RigidBodyInertia Ii;
        double q_;
    };
}//end of namespace
#endif
