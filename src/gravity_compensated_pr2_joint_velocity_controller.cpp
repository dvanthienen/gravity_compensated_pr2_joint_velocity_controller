/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Dominick Vanthienen <first dot last at kuleuven dot be>
 *  Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "gravity_compensated_pr2_joint_velocity_controller/gravity_compensated_pr2_joint_velocity_controller.h"
#include "pluginlib/class_list_macros.h"

PLUGINLIB_DECLARE_CLASS(gravity_compensated_pr2_joint_velocity_controller, GravityCompensatedPr2JointVelocityController, controller::GravityCompensatedPr2JointVelocityController, pr2_controller_interface::Controller)

using namespace std;

namespace controller {

GravityCompensatedPr2JointVelocityController::GravityCompensatedPr2JointVelocityController()
: joint_state_(NULL), command_(0), robot_(NULL), last_time_(0), loop_count_(0)
{
}

GravityCompensatedPr2JointVelocityController::~GravityCompensatedPr2JointVelocityController()
{
  sub_command_.shutdown();
}

bool GravityCompensatedPr2JointVelocityController::init(pr2_mechanism_model::RobotState *robot, const std::string &joint_name,
				   const control_toolbox::Pid &pid)
{
  assert(robot);
  robot_ = robot;
  last_time_ = robot->getTime();

  //TODO: when is this version of init used? is it used? if so is the urdf_file parameter set? if not: error
  if(urdf_file_.empty()){
    ROS_ERROR("Making use of the first init version, without urdf_file parameter set");
    return false;
  }

  joint_state_ = robot_->getJointState(joint_name);
  if (!joint_state_)
  {
    ROS_ERROR("GravityCompensatedPr2JointVelocityController could not find joint named \"%s\"\n",
              joint_name.c_str());
    return false;
  }

  pid_controller_ = pid;

  return true;
}

bool GravityCompensatedPr2JointVelocityController::init(pr2_mechanism_model::RobotState *robot, ros::NodeHandle &n)
{
  assert(robot);
  node_ = n;
  robot_ = robot;

  if(!initGravityCompensation(robot, node_)){
    ROS_ERROR("Initialisation of the gravity compensation failed");
    return false;
  }

  std::string joint_name;
  if (!node_.getParam("joint", joint_name)) {
    ROS_ERROR("No joint given (namespace: %s)", node_.getNamespace().c_str());
    return false;
  }
  if (!(joint_state_ = robot->getJointState(joint_name)))
  {
    ROS_ERROR("Could not find joint \"%s\" (namespace: %s)",
              joint_name.c_str(), node_.getNamespace().c_str());
    return false;
  }

  if (!pid_controller_.init(ros::NodeHandle(node_, "pid")))
    return false;

  controller_state_publisher_.reset(
    new realtime_tools::RealtimePublisher<pr2_controllers_msgs::JointControllerState>
    (node_, "state", 1));

  sub_command_ = node_.subscribe<std_msgs::Float64>("command", 1, &GravityCompensatedPr2JointVelocityController::setCommandCB, this);

  return true;
}

bool GravityCompensatedPr2JointVelocityController::initGravityCompensation(pr2_mechanism_model::RobotState *robot, ros::NodeHandle &n){
  //get the necessary parameters from the parameter server
  if(!n.getParam("/simplified_gripper_robot_description", urdf_file_)){
    ROS_ERROR("No simplified gripper robot description urdf file on the parameter server");
    return false;
  }
  if(!n.getParam("arm_root",arm_root_)){
    ROS_ERROR("No %s/arm_root defined on the parameter server",n.getNamespace().c_str());
    return false;
  }
  if(!n.getParam("arm_tip",arm_tip_)){
    ROS_ERROR("No %s/arm_tip defined on the parameter server",n.getNamespace().c_str());
    return false;
  }

  //parse the urdf file to a KDL tree and cut the arm out of it
  if (!kdl_parser::treeFromString(urdf_file_, robot_tree_)){
    ROS_ERROR("Failed to construct kdl tree with simplified robot description");
    return false;
  }
  KDL::Chain arm;
  if (!robot_tree_.getChain(arm_root_,arm_tip_, arm_)){
    ROS_ERROR("Couldn't get arm chain between %s and %s from robot tree",arm_root_.c_str(),arm_tip_.c_str());
    return false; 
  }
  num_jnts_ = arm_.getNrOfJoints();
  if(num_jnts_<1){
    ROS_ERROR("Got no joints in the arm (chain from KDL tree)");
  }

  //resize vectors, definitely not RT
  q_.resize(num_jnts_);
  torques_.resize(num_jntsi_);
  KDL::SetToZero(torques);

  //initialize a solver
  id_solver_gravity_ = KDL::ChainIdSolver_gravity(arm_, gravity_);

  return true;
}


void GravityCompensatedPr2JointVelocityController::setGains(const double &p, const double &i, const double &d, const double &i_max, const double &i_min)
{
  pid_controller_.setGains(p,i,d,i_max,i_min);

}

void GravityCompensatedPr2JointVelocityController::getGains(double &p, double &i, double &d, double &i_max, double &i_min)
{
  pid_controller_.getGains(p,i,d,i_max,i_min);
}

std::string GravityCompensatedPr2JointVelocityController::getJointName()
{
  return joint_state_->joint_->name;
}

// Set the joint velocity command
void GravityCompensatedPr2JointVelocityController::setCommand(double cmd)
{
  command_ = cmd;
}

// Return the current velocity command
void GravityCompensatedPr2JointVelocityController::getCommand(double  & cmd)
{
  cmd = command_;
}

void GravityCompensatedPr2JointVelocityController::update()
{
  assert(robot_ != NULL);
  ros::Time time = robot_->getTime();

  double error = joint_state_->velocity_ - command_;
  dt_ = time - last_time_;
  double command = pid_controller_.updatePid(error, dt_);
  joint_state_->commanded_effort_ += command;
  
  //TODO fill in q_ and do something with torques_
  //calculate feedforward torques
  ret_ = id_solver_gravity_.CartToJnt(q_,torques_);
  //if(ret!=0){
  //  ROS_ERROR("Inverse dynamics solver failed and returned ");
  //}

  if(loop_count_ % 10 == 0)
  {
    if(controller_state_publisher_ && controller_state_publisher_->trylock())
    {
      controller_state_publisher_->msg_.header.stamp = time;
      controller_state_publisher_->msg_.set_point = command_;
      controller_state_publisher_->msg_.process_value = joint_state_->velocity_;
      controller_state_publisher_->msg_.error = error;
      controller_state_publisher_->msg_.time_step = dt_.toSec();
      controller_state_publisher_->msg_.command = command;

      double dummy;
      getGains(controller_state_publisher_->msg_.p,
               controller_state_publisher_->msg_.i,
               controller_state_publisher_->msg_.d,
               controller_state_publisher_->msg_.i_clamp,
               dummy);
      controller_state_publisher_->unlockAndPublish();
    }
  }
  loop_count_++;

  last_time_ = time;
}

void GravityCompensatedPr2JointVelocityController::setCommandCB(const std_msgs::Float64ConstPtr& msg)
{
  command_ = msg->data;
}

} // namespace
