Instructions to run the gravity compensated pr2 joint velocity controller
------------

*  load the adapted urdf file in the parameter server

```
#!bash
roslaunch gravity_compensated_pr2_joint_velocity_controller pr2_simple_gripper_urdf.launch

```

* launch the controllers

```
#!bash
roslaunch gravity_compensated_pr2_joint_velocity_controller controllers_onlyp.launch

```

